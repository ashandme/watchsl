package database

import (
	// "fmt"
	"log"

	"github.com/dgraph-io/badger"
	"github.com/dgraph-io/badger/options"
	// "fmt"
)

var (
	dataDirectory = "/tmp/badger"
)

type Database interface {
	Set(key, value []byte) error
	Delete(key []byte) error
	DeleteAll() error
	Get(key []byte) ([]byte, error)
	GetAllKeys() ([][]byte, error)
	Close() error
}

type Badger struct {
	db *badger.DB
}

func Open(path string) (Database, error) {
	if path == "" {
		path = dataDirectory
	}

	d := Badger{}
	opts := badger.DefaultOptions(path)
	opts.Logger = nil
	opts.ValueLogLoadingMode = options.FileIO
	db, err := badger.Open(opts)
	if err != nil {
		// log.Printf("[ERROR] db new %s", err)
		return d, err
	}
	d.db = db
	return d, nil
}

func (d Badger) Close() error {
	err := d.db.Close()
	if err != nil {
		return err
	}

	return nil
}

func (d Badger) Set(key []byte, value []byte) error {
	err := d.db.Update(func(txn *badger.Txn) error {
		txn.Set(key, value)
		return nil
	})
	if err != nil {
		log.Printf("[ERROR] db set %s", err)
		return err
	}
	return nil
}

func (d Badger) Delete(key []byte) error {
	err := d.db.Update(func(txn *badger.Txn) error {
		txn.Delete(key)
		return nil
	})
	if err != nil {
		log.Printf("[ERROR] db del %s", err)
		return err
	}
	return nil
}

func (d Badger) DeleteAll() error {
	err := d.db.DropAll()
	if err != nil {
		log.Printf("[ERROR] db purge %s", err)
		return err
	}

	return nil
}

func (d Badger) Get(key []byte) ([]byte, error) {
	var valCopy []byte
	err := d.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get(key)
		if err != nil {
			// log.Printf("[ERROR] db get %s", err)
			return err
		}

		err = item.Value(func(val []byte) error {
			return nil
		})
		if err != nil {
			log.Printf("[ERROR] db get %s", err)
			return err
		}

		valCopy, err = item.ValueCopy(nil)

		return nil
	})
	if err != nil {
		empty := []byte("")
		return empty, err
	}
	return valCopy, nil
}

func (d Badger) GetAllKeys() ([][]byte, error) {
	keys := [][]byte{}
	err := d.db.View(func(txn *badger.Txn) error {
		defer txn.Discard()
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			// k := item.Key()
			k := item.KeyCopy(nil)
			keys = append(keys, k)
			// fmt.Printf("%s\n",k)
			// fmt.Println(i)
		}
		return nil
	})
	if err != nil {
		log.Printf("[ERROR] db getAll %s", err)
		return keys, err

	}
	// println(len(keys))
	// fmt.Println("---")
	// for _, k := range keys {
	// 	fmt.Printf("%s\n", k)
	// }
	return keys, nil
}
