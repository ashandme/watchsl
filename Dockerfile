# builder image
FROM golang:alpine as builder
RUN mkdir /build
ADD *.go /build/
ADD database /build/database
WORKDIR /build
RUN go mod init gitlab.com/IvanTurgenev/watchsl ; go mod tidy
RUN CGO_ENABLED=0 GOOS=linux go build -a -o main .


# generate clean, final image for end users
FROM alpine:latest
RUN mkdir /app
WORKDIR /app
COPY --from=builder /build/main .
ADD config.ini .
# executable
ENTRYPOINT [ "./main" ]
