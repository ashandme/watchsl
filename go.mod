module gitlab.com/IvanTurgenev/watchsl

go 1.14

require (
	github.com/DisgoOrg/disgo v0.7.0
	github.com/DisgoOrg/log v1.1.2 // indirect
	github.com/DisgoOrg/snowflake v1.0.1 // indirect
	github.com/akamensky/argparse v1.3.0
	github.com/bwmarrin/snowflake v0.3.0 // indirect
	github.com/dgraph-io/badger v1.6.1
	github.com/sirupsen/logrus v1.8.1
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/turnage/graw v0.0.0-20200404033202-65715eea1cd0
	gopkg.in/ini.v1 v1.57.0
)
