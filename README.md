# watchsl
## gotify notifications for new posts in multiple subreddits with specific keywords

[![pipeline status](https://gitlab.com/IvanTurgenev/watchsl/badges/master/pipeline.svg)](https://gitlab.com/IvanTurgenev/watchsl/-/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/IvanTurgenev/watchsl)](https://goreportcard.com/report/gitlab.com/IvanTurgenev/watchsl)

![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/GPLv3_Logo.svg/200px-GPLv3_Logo.svg.png "Title Text")

## Use
first you need to create a [reddit aplication](https://www.reddit.com/prefs/apps) in order to get access to the API.
down below the page, click in "create another app...". Give it a name and a redirect url like "https://example.com", don't forget to choose the *script* option in App type

![alt text](images/create.png)

Then copy your app's client ID and the client secret. paste it in config.ini

```ini
[reddit_bot]
...
client_ID = <APP_CLIENT_ID>
client_secret = <APP_CLIENT_SECRET>
username = <MY_REDDIT_USERNAME>
password = <MY_REDDIT_PASSWORD>
...
```

All you need now is creating a new app for your [gotify server](https://github.com/gotify/server)

![alt text](images/gotify.png)

```ini
[gotify]
server_url = http://<GOTIFY_SERVER_DOMAIN>/message?token= 
app_token  = <MY_GOTIFY_APP_TOKEN>
```
