package main

import "testing"

func TestEmpty(t *testing.T) {
    got := Empty("")
    want := " "

    if got != want {
        t.Errorf("got %q want %q", got, want)
    }
}