package main

import (
	"bytes"
	"fmt"
	"strconv"

	"github.com/akamensky/argparse"

	// "log"
	"os"

	// "regexp"
	// "crypto/tls"

	"encoding/gob"
	"errors"

	// "sort"
	"os/signal"
	"strings"
	"syscall"
	"time"
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/turnage/graw"
	"github.com/turnage/graw/reddit"
	db "gitlab.com/IvanTurgenev/watchsl/database"
	"gopkg.in/ini.v1"
	"github.com/DisgoOrg/disgo/rest"
	"github.com/DisgoOrg/disgo/discord"
	"github.com/DisgoOrg/snowflake"
	logd "github.com/DisgoOrg/log"
	"github.com/DisgoOrg/disgo/webhook"
	
)

type watchBot struct {
	bot reddit.Bot
	db  db.Database
}

type postDb struct {
	Permalink     string
	Title         string
	URL           string
	SelfText      string
	Author        string
	LinkFlairText string
	Subreddit     string
	CreatedUTC    time.Time
	ID            string
}

var subredditsToWatchMap = map[string][]string{}

var botConfig = reddit.BotConfig{}

var webhookID string

var webhookToken string

var client = &webhook.Client{}

const dbname string = "badger.db"

var subredditsToWatch []string

var globalKeys [][]byte

func topostDb(p *reddit.Post) postDb {
	dbp := postDb{
		Title:         p.Title,
		SelfText:      p.SelfText,
		URL:           p.URL,
		Permalink:     p.Permalink,
		Author:        p.Author,
		LinkFlairText: p.LinkFlairText,
		Subreddit:     p.Subreddit,
		CreatedUTC:    time.Unix(int64(p.CreatedUTC), 0),
		ID:            p.ID,
	}
	return dbp

}

//Empty check if string is empty if empty return " " if not return string
func Empty(s string) string {
	if len(strings.TrimSpace(s)) == 0 {
		return (" ")
	}
	return s
}

func contain(post postDb) bool {
	// log.Info("Harvested post: " + post.URL)
	tempPostKeywords := append(strings.Fields(post.Title), strings.Fields(post.LinkFlairText)...)
	tempKeywords := subredditsToWatchMap[post.Subreddit]

	if len(tempKeywords) == 0 {
		return true
	}

	for _, keyword := range tempKeywords {
		for _, postKeyword := range tempPostKeywords {
			if strings.Contains(strings.ToUpper(postKeyword), strings.ToUpper(keyword)) {
				return true
			}
		}
	}
	return false
}

func encodedb(p postDb) []byte {
	var b bytes.Buffer
	e := gob.NewEncoder(&b)
	if err := e.Encode(p); err != nil {
		panic(err)
	}
	return b.Bytes()

}

func (bot *watchBot) Post(newPost *reddit.Post) error {
	if globalKeys != nil {
		keys, err := bot.db.GetAllKeys()
		if err != nil {
			log.Error("Could not get keys: ", err)
			panic(Exit{1})
		}
		globalKeys = keys
		// for i, key := range keys {
		// 	tempPost, err := getdb(key, bot.db)
		// 	if err != nil {
		// 		log.Error("Could not retrieve post with ID: " + string(key))
		// 		log.Error(err)
		// 		panic(Exit{1})
		// 	}
		// 	log.Printf(strconv.Itoa(i+1)+": ", tempPost.CreatedUTC)
		// }

	}

	newDBPost := topostDb(newPost)
	// _, err := getdb([]byte(newDBPost.ID), r.db)
	if contain(newDBPost) {
		setdb([]byte(newDBPost.ID), newDBPost, bot.db)
		sendNotification(newDBPost, 8)

		log.Debug("New post: " + newDBPost.URL)
		globalKeys = append(globalKeys, []byte(newDBPost.ID))
		deldb(globalKeys[0], bot.db)
		globalKeys = globalKeys[1:]
		// log.Debug("Number of keys in memory: " + strconv.Itoa(len(globalKeys)))

	}
	return nil
}

func opendb() db.Database {
	log.Info("Opening database...")
	d, err := db.Open(dbname)
	if err != nil {
		log.Error("Could not create or open database: ", err)
		panic(Exit{1})
	}
	return d
}

func setdb(key []byte, value postDb, db db.Database) {
	_, err0 := getdb(key, db)
	if err0 == nil {
		return
	}
	if err := db.Set(key, encodedb(value)); err != nil {
		fmt.Println("I didn't set any data")
	}
}

func getdb(key []byte, db db.Database) (postDb, error) {
	value, err := db.Get(key)
	if err != nil {
		return postDb{}, errors.New("Post doesnt exist in database")
	}

	var postdbDecode postDb
	d := gob.NewDecoder(bytes.NewReader(value))
	if err := d.Decode(&postdbDecode); err != nil {
		log.Error("Could not decode post iD: " + string(key))
		log.Error(err)
		panic(Exit{1})
	}
	// fmt.Println(value)
	return postdbDecode, nil
}

func deldb(key []byte, db db.Database) {
	if err := db.Delete(key); err != nil {
		log.Error("Could not delete from DB: ", err)
		panic(Exit{1})
	}
}

func tomulti() string {

	templj := strings.Join(subredditsToWatch[:], "+")
	return "/r/" + templj + "/new"
}

func getNewestDBPostTime(db db.Database) time.Time {
	keys, err := db.GetAllKeys()
	if err != nil {
		log.Error("Could not get keys: ", err)
		panic(Exit{1})
	}

	newestDbPostTime := time.Time{}

	for _, key := range keys {
		tempPost, err := getdb(key, db)
		if err != nil {
			log.Error("Could not retrieve post with ID: " + string(key))
			log.Error(err)
			panic(Exit{1})
		}
		if newestDbPostTime == (time.Time{}) {
			newestDbPostTime = tempPost.CreatedUTC
		} else {
			if tempPost.CreatedUTC.After(newestDbPostTime) {
				newestDbPostTime = tempPost.CreatedUTC
			}
		}

	}

	return newestDbPostTime

}

func sendNotification(post postDb, priority int) {
	//_, err = sendnot(tempPost.Title, tempPost.Author+"\n"+tempPost.URL+"\n"+tempPost.SelfText, "8")
	//response, err := http.PostForm(servURLToken, url.Values{"message": {Empty(post.Subreddit + "\n" + post.Author + "\n" + post.URL + "\n" + post.SelfText)}, "title": {post.Title}, "priority": {strconv.Itoa(priority)}})
	//log.Debug("Gotify response :", response)
	//if err != nil {
	//	log.Error("Could not send notification: ", err)
	//	log.Error("Response: ", response.StatusCode)
	//}
	//defer response.Body.Close()

	//if response.StatusCode != http.StatusOK {
	//	log.Error("Non-OK HTTP status from server: ", response.StatusCode)

	//}
	emb :=[]discord.Embed{ discord.NewEmbedBuilder().
		SetTitle(post.Title).
		SetAuthorName(post.Author).
		SetDescriptionf(post.SelfText).
		SetURL(post.URL).
		Build(),}
	if _, err := client.CreateEmbeds(emb, rest.WithDelay(2*time.Second),); err != nil {
		logd.Errorf("error sending message: %s", err)
	}
	//if _, err := client.CreateMessage(discord.NewWebhookMessageCreateBuilder().
		
	//	SetContentf(post.SelfText).
	//	Build(),
	//	rest.WithDelay(2*time.Second),); err != nil {
	//	logd.Errorf("error sending message: %s", err)
	//}
}

func harvpost(db db.Database) {
	log.Info("Harvesting posts...")
	bot, err := reddit.NewBot(botConfig)
	if err != nil {
		log.Error("Could not create bot from config: ", err)
		panic(Exit{1})
	}
	var harvest reddit.Harvest
	for {
		harv, err := bot.Listing(tomulti(), "")
		if err != nil {
			log.Warn("Could not harvest posts, retrying in 1 minute: ", err)
			time.Sleep(60 * time.Second)
		} else {
			harvest = harv
			break

		}

	}
	var harvestedPosts []postDb
	for i := 0; i < 100; i++ {
		tmpHarvestPost := topostDb(harvest.Posts[i])
		harvestedPosts = append(harvestedPosts, tmpHarvestPost)

	}

	newPostCount, oldPostCount := 0, 0

	oldestNewPost := postDb{}
	newestDBPostTime := getNewestDBPostTime(db)

	for _, tempPost := range harvestedPosts {
		if oldestNewPost != (postDb{}) {
			if tempPost.CreatedUTC.Before(oldestNewPost.CreatedUTC) {
				oldestNewPost = tempPost
			}

		} else {
			oldestNewPost = tempPost
		}

		oldPost, err := getdb([]byte(tempPost.ID), db)
		if err != nil && contain(tempPost) && tempPost.CreatedUTC.After(newestDBPostTime) {
			log.Debug("New post: " + tempPost.URL)
			newPostCount++
			setdb([]byte(tempPost.ID), tempPost, db)
			sendNotification(tempPost, 8)
		}
		if (postDb{}) != oldPost {
			log.Debug("Old post: " + tempPost.URL)
			oldPostCount++
		}

	}

	log.Debug("New posts found: " + strconv.Itoa(newPostCount))
	log.Debug("Old posts found: " + strconv.Itoa(oldPostCount))

	cleandb(db, false, oldestNewPost.CreatedUTC)
}

func cleandb(db db.Database, cleanDatabase bool, oldestNewPost time.Time) {
	keys, err := db.GetAllKeys()
	if err != nil {
		log.Error("Could not get keys: ", err)
		panic(Exit{1})
	}

	if cleanDatabase {
		log.Info("Cleaning all database...")
		for _, key := range keys {
			deldb(key, db)
		}
		log.Info("Exiting...")
		panic(Exit{0})

	}
	log.Debug("Deleting old posts in database before: " + oldestNewPost.Format("2006-01-02 15:04:05"))
	deletedPosts := 0

	for _, key := range keys {
		post, err := getdb(key, db)
		if err != nil {
			log.Error("Could not retrieve post with ID: " + string(key))
			log.Error(err)
			panic(Exit{1})
		}
		if post.CreatedUTC.Before(oldestNewPost) {
			deldb(key, db)
			deletedPosts++
		}
	}

	log.Debug("Deleted posts: ", strconv.Itoa(deletedPosts))

}

func creaDefConf() {
	cfg := ini.Empty()
	cfg.Section("reddit_bot").Key("user_agent").Comment = `# User agent for bot`
	cfg.Section("reddit_bot").NewKey("user_agent", `Debian:watchsl:v0.1.0 (by /u/xxx)`)
	cfg.Section("reddit_bot").Key("client_ID").Comment = `# Reddit api app ID`
	cfg.Section("reddit_bot").NewKey("client_ID", `xxx--xxx-xxx`)
	cfg.Section("reddit_bot").Key("client_secret").Comment = `# Reddit api app secret`
	cfg.Section("reddit_bot").NewKey("client_secret", `xxxxxxxxxxx`)
	cfg.Section("reddit_bot").Key("username").Comment = `# Reddit bot account username`
	cfg.Section("reddit_bot").NewKey("username", `xxx`)
	cfg.Section("reddit_bot").Key("password").Comment = `# Reddit bot account password`
	cfg.Section("reddit_bot").NewKey("password", `xxx`)
	cfg.Section("reddit_bot").Key("update_every").Comment = `# Rate limit is 1 every 2 seconds, for one every 60, 60, one every 30, 30, one every 2 seconds, 2 which is max limit before getting ratelimit errors`
	cfg.Section("reddit_bot").NewKey("update_every", "60")
	cfg.Section("gotify").Key("server_url").Comment = `# If running watchsl in docker, localhost assumes network mode host for watchsl`
	cfg.Section("webhook").NewKey("webhook_id", "XXXXX")
	cfg.Section("webhook").NewKey("webhook_token", "XXXXXXX")
	cfg.Section("subreddits").Comment = `# Subreddits to monitor add more if needed, careful with subredditnames`
	cfg.Section("subreddits").Key("slavelabour").Comment = `# Monitor slavelabour subreddit with these keywords to look for in new posts titles and flairs, leave empty for all, case insensitive`
	cfg.Section("subreddits").NewKey("slavelabour", "TASK, HIRING")
	cfg.Section("subreddits").Key("Jobs4Bitcoins").Comment = `# Monitor Jobs4Bitcoins subreddit with these keywords to look for in new posts titles and flairs, leave empty for all, case insensitive`
	cfg.Section("subreddits").NewKey("Jobs4Bitcoins", "TASK, HIRING")
	cfg.Section("subreddits").Key("AskReddit").Comment = `# Monitor AskReddit subreddit for new posts, empty for all posts`
	cfg.Section("subreddits").NewKey("AskReddit", "")

	if confileExists("config.example.ini") {
	} else {
		log.Info("Creating example config file: config.example.ini")
		err := cfg.SaveTo("config.example.ini")
		if err != nil {
			log.Error("Could not save example config file: ", err)
			panic(Exit{1})
		}

	}
	if confileExists("config.ini") {

	} else {
		log.Info("Config file doesnt exist creating: config.ini")
		err := cfg.SaveTo("config.ini")
		if err != nil {
			log.Error("Could not save example config file: ", err)
			panic(Exit{1})
		}
		log.Warn("Exiting edit new config file, and start again")
		panic(Exit{0})
	}
}

func confileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func runbot(db db.Database) {
	bot, err := reddit.NewBot(botConfig)
	if err != nil {
		log.Error("Failed to start bot, from configuration: ", err)
		panic(Exit{1})
	}

	cfg := graw.Config{Subreddits: subredditsToWatch}
	handler := &watchBot{bot: bot, db: db}
	for {
		log.Info("Starting bot...")
		if _, wait, err := graw.Run(handler, bot, cfg); err != nil {
			log.Error("Failed to start bot: ", err)
			log.Info("Restarting bot in 60 seconds...")
			time.Sleep(60 * time.Second)
		} else {
			log.Info("Bot started")
			err = wait()
			if err != nil {
				log.Warn("Bot failed: ", err)
			}
			log.Info("Restarting bot in 60 seconds...")
			time.Sleep(60 * time.Second)
		}
	}
}
func loadconf() {
	log.Info("Reading configuration file..")
	cfgini, err := ini.Load("config.ini")
	if err != nil {
		log.Error("Failed reading config file: ", err)
		panic(Exit{1})
	}
	botConfig.Agent = cfgini.Section("reddit_bot").Key("user_agent").String()
	botConfig.App.ID = cfgini.Section("reddit_bot").Key("client_ID").String()
	botConfig.App.Secret = cfgini.Section("reddit_bot").Key("client_secret").String()
	botConfig.App.Username = cfgini.Section("reddit_bot").Key("username").String()
	botConfig.App.Password = cfgini.Section("reddit_bot").Key("password").String()
	var update_every int
	update_every, err = cfgini.Section("reddit_bot").Key("update_every").Int()
	if err != nil {
		log.Warn("Failed parsing update every, defaulting to 30: ", err)
		update_every = 30
	}
	botConfig.Rate = time.Duration(update_every) * time.Second
	webhookID = cfgini.Section("webhook").Key("webhook_id").String()
	webhookToken = cfgini.Section("webhook").Key("webhook_token").String()
	subredditsToWatchSection, err := cfgini.GetSection("subreddits")
	if err != nil {
		log.Error("Failed parsing subreddits section: ", err)
		panic(Exit{1})
	}
	subredditsToWatch = subredditsToWatchSection.KeyStrings()
	// subredditsToWatch = cfgini.Section("reddit").Key("subredditsToWatch").Strings(",")
	if len(subredditsToWatch) == 0 {
		log.Error("Empty subreddits, add subreddits")
		panic(Exit{1})
	}
	for _, subreddit := range subredditsToWatch {
		subredditsToWatchMap[subreddit] = subredditsToWatchSection.Key(subreddit).Strings(",")
	}
}

type Exit struct{ Code int }

// exit code handler
func handleExit() {
	if e := recover(); e != nil {
		if exit, ok := e.(Exit); ok {
			os.Exit(exit.Code)
		}
		panic(e) // not an Exit, bubble up
	}
}

func options() {
	// Create new parser object
	parser := argparse.NewParser("watchsl", "Watches reddit filtering posts by subreddits and keywords and sending notifications with gotify")
	// Create string flag
	cleanDatabase := parser.Flag("c", "clean", &argparse.Options{Help: "Cleans database"})
	debug := parser.Flag("d", "debug", &argparse.Options{Help: "Sets log level to debug"})
	// Parse input
	err := parser.Parse(os.Args)
	if err != nil {
		// In case of error print error and print usage
		// This can also be done by passing -h or --help flags
		fmt.Print(parser.Usage(err))
		panic(Exit{1})
	}
	// Finally print the collected string
	if *cleanDatabase {
		db := opendb()
		defer db.Close()
		cleandb(db, true, time.Time{})
	}

	if *debug {
		log.SetLevel(log.DebugLevel)
	}

}

func main() {
	defer handleExit()
	options()
	creaDefConf()
	loadconf()
	db := opendb()
	defer db.Close()
	client = webhook.NewClient(snowflake.Snowflake(webhookID), webhookToken)
	harvpost(db)
	defer client.Close(context.TODO())
	go runbot(db)
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc
	log.Info("Exiting...")

}
